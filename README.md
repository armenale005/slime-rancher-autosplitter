# THIS PROJECT HAS BEEN MOVED TO GITHUB
This is a work-in-progress autosplitter for Slime Rancher for use with Livesplit.

Currently supported features:
- Start the timer on new game
- Split on credits start

Planned features: (in order of priority)
- Split when popping Gordos (non-event)
- Split when opening the Ancient Ruins gate
- Reimplement splitting when getting/using keys